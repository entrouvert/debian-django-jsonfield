#!/usr/bin/python

import os
import sys
sys.path.insert(0, os.getcwd())

from django.conf import settings
settings.configure(
    INSTALLED_APPS=['jsonfield'],
    DATABASES={
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'tests.db',
            'USER': '',
            'PASSWORD': '',
            'HOST': '',
            'PORT': '',
        }
    }
)

import django
django.setup()

from django.core import management
management.call_command('test')
